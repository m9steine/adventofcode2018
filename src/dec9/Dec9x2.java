package dec9;

import java.sql.Timestamp;
import java.util.ArrayDeque;
import java.util.Arrays;

class CircleDeque<T> extends ArrayDeque<T> {

	public static void main(String[] args) {
		Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
		System.out.println(timeStamp);
		System.out.println(game(403, 71920 * 100));
		timeStamp = new Timestamp(System.currentTimeMillis());
		System.out.println(timeStamp);
	}

	static long game(int players, int end) {
		CircleDeque<Integer> circle = new CircleDeque<>();
		circle.addFirst(0);
		long[] scores = new long[players];
		for (int i = 1; i <= end; i++) {
			if (i % 23 == 0) {
				circle.rotate(-7);
				scores[i % players] += i + circle.pop();
			} else {
				circle.rotate(2);
				circle.addLast(i);
			}
		}
		return Arrays.stream(scores).max().getAsLong();
	}

	void rotate(int num) {
		if (num == 0)
			return;
		if (num > 0) {
			for (int i = 0; i < num; i++) {
				T t = this.removeLast();
				this.addFirst(t);
			}
		} else {
			for (int i = 0; i < Math.abs(num) - 1; i++) {
				T t = this.remove();
				this.addLast(t);
			}
		}
	}
}