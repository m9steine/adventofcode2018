package dec9;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections.list.TreeList;

import utils.InputUtils;

public class Dec9x1 {

	private static final String DAY9_INPUT = "day9/input.txt";
	private static final String DAY9_TEST_INPUT = "day9/test.txt";

	private static InputUtils helper = new InputUtils();
	private static List<Integer> inputInt = new ArrayList<>();
	private static int indexOfCurrentMarble;
	private static int[] players;

	public static void main(String[] args) {
		Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
		System.out.println(timeStamp);
		//List<String> testInput = helper.getInput(DAY9_TEST_INPUT);
		//List<String> input = helper.getInput(DAY9_INPUT);
		// TODO get the values from the input;
		int numberOfPlayers = 403;
		int numberOfMarbles = 71920;

		players = new int[numberOfPlayers+1];
		int[] marbles = new int[numberOfMarbles];

		//LinkedList<Integer> playCircle = new LinkedList<Integer>();
		TreeList playCircle = new TreeList();

		playCircle.add(0);

		makeMove(playCircle, numberOfMarbles, numberOfPlayers);

		int maxScore = 0;
		for (int index = 0; index < players.length; index++) {
			if (players[index] > maxScore) {
				maxScore = players[index];
			}
		}
		
		System.out.println(maxScore);
		timeStamp = new Timestamp(System.currentTimeMillis());
		System.out.println(timeStamp);

	}

	private static void makeMove(TreeList playCircle, int numberOfMarbles, int numberOfPlayers) {
		
		for (int currentMarble = 1; currentMarble < numberOfMarbles; currentMarble++) {
			if (currentMarble % 100000 == 0) {
				System.out.println(currentMarble);
			}
			int currentPlayer = ((currentMarble - 1) % numberOfPlayers) + 1;
			int indexOfCurrentMarble = 0;
			int jndex = playCircle.indexOf(currentMarble - 1);
			if (jndex >= 0) {
				indexOfCurrentMarble = jndex;
			} else {
				int size = playCircle.size();
				indexOfCurrentMarble = ((playCircle.indexOf(currentMarble - 2) - 6)+size)%size;
			}
			int indexOfNextNextMarble = indexOfCurrentMarble + 2;
			if (currentMarble % 23 == 0) {
				int size = playCircle.size();
				players[currentPlayer] += currentMarble;
				int marbleToRemove = (int) playCircle.get(((indexOfCurrentMarble - 7) + size) % size);
				players[currentPlayer] += marbleToRemove;
				playCircle.remove(((indexOfCurrentMarble - 7) + size) % size);
				//players[currentPlayer] += playCircle.remove(playCircle.indexOf(playCircle.get(((indexOfCurrentMarble - 7) + size) % size)));

			} else {
				if (indexOfCurrentMarble == 0 || indexOfNextNextMarble == playCircle.size()) {
					playCircle.add(currentMarble);
				} else {
					playCircle.add((indexOfNextNextMarble) % playCircle.size(), (currentMarble));
				}
			}
		}
	}

}
