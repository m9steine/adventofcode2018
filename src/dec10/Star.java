package dec10;

public class Star {
	private int posX;
	private int posY;
	
	private int veloX;
	private int veloY;
	
	private boolean hasNeighbour;
	
	Star(int x, int y, int xv, int yv){
		this.posX = x;
		this.posY = y;
		this.veloX = xv;
		this.veloY = yv;
		
	}

	public int getxPos() {
		return posX;
	}

	public void setxPos(int xPos) {
		this.posX = xPos;
	}

	public int getyPos() {
		return posY;
	}

	public void setyPos(int yPos) {
		this.posY = yPos;
	}

	public int getxVelo() {
		return veloX;
	}

	public void setxVelo(int xVelo) {
		this.veloX = xVelo;
	}

	public int getyVelo() {
		return veloY;
	}

	public void setyVelo(int yVelo) {
		this.veloY = yVelo;
	}

	public boolean isHasNeighbour() {
		return hasNeighbour;
	}

	public void setHasNeighbour(boolean hasNeighbour) {
		this.hasNeighbour = hasNeighbour;
	}
	
	

}
