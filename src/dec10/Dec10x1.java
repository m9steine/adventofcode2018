package dec10;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import utils.InputUtils;

public class Dec10x1 {

	private static final String DAY10_INPUT = "day10/input.txt";
	private static final String DAY10_TEST_INPUT = "day10/test.txt";
	private static InputUtils helper = new InputUtils();
	private static List<Star> stars = new ArrayList<>();

	// private static Pattern point = Pattern.compile("position=\\<(.*?)\\>");
	// private static Pattern velocity = Pattern.compile("velocity=\\<(.*?)\\>");

	public static void main(String[] args) throws IOException {

		List<String> testInput = helper.getInput(DAY10_TEST_INPUT);
		List<String> input = helper.getInput(DAY10_INPUT);
		// char [][] coordinates = new char[51_000][51_000];
		char[][] coordinates = new char[250][250];
		int second = 1;
		fillStars(input);
		hasNeighbor();

		while (true) {

			moveStars(stars);
			hasNeighbor();
			if (getNumOfNeighbors() < 300) {
			} else {
				System.setOut(new PrintStream(new File("E:/output/output"+second+".txt")));
				drawStars(stars, coordinates);
				break;
			}
			second++;
		}
	}

	private static int getNumOfNeighbors() {
		int neighbors = 0;
		for (Star current : stars) {
			if (current.isHasNeighbour()) {
				neighbors++;
			}
		}
		return neighbors;
	}

	private static void hasNeighbor() {
		for (Star current : stars) {
			boolean hasNeighbor = false;
			for (Star next : stars) {
				if (!next.equals(current) && (Math.abs(current.getxPos() - next.getxPos()) <= 1
						&& Math.abs(current.getyPos() - next.getyPos()) <= 1)) {
					hasNeighbor = true;
				}
			}

			current.setHasNeighbour(hasNeighbor);
		}

	}

	private static void moveStars(List<Star> stars2) {
		for (Star current : stars) {
			current.setxPos(current.getxPos() + current.getxVelo());
			current.setyPos(current.getyPos() + current.getyVelo());
		}

	}

	private static void drawStars(List<Star> stars2, char[][] coordinates) {
		for (int i = 0; i < coordinates.length; i++) {
			for (int j = 0; j < coordinates[i].length; j++) {
				coordinates[i][j] = '.';
			}
		}

		for (Star current : stars) {
			coordinates[current.getxPos()][current.getyPos()] = '#';
		}

		for (int i = 0; i < coordinates.length; i++) {
			for (int j = 0; j < coordinates[i].length; j++) {
				System.out.print(coordinates[j][i]);
			}
			System.out.println();
		}

	}

	private static void fillStars(List<String> testInput) {
		Pattern point = Pattern.compile("position=\\<(.*?)\\>");
		Pattern velocity = Pattern.compile("velocity=\\<(.*?)\\>");
		for (String line : testInput) {
			Matcher matcherPoint = point.matcher(line);
			Matcher matcherVelo = velocity.matcher(line);
			if (matcherPoint.find() && matcherVelo.find()) {
				String[] starCoordinate = matcherPoint.group(1).split(",");
				String[] starVelocity = matcherVelo.group(1).split(",");
				Star star = new Star(Integer.parseInt(starCoordinate[0].trim()),
						Integer.parseInt(starCoordinate[1].trim()), Integer.parseInt(starVelocity[0].trim()),
						Integer.parseInt(starVelocity[1].trim()));
				stars.add(star);
			}
		}

	}

}
