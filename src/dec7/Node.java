package dec7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Node {

	char myself;
	Set<Character> parent = new HashSet<>();

	public Node(char myself, char parent) {
		super();
		this.myself = myself;
		this.parent.add(parent);
	}
	
	public Node(char parent) {
		super();
		this.myself = parent;
	}

	public void sortChildren() {
		List<Character> sortedChilds = new ArrayList<>(this.parent);
		Collections.sort(sortedChilds);
		this.parent = new HashSet<>(sortedChilds);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + myself;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (myself != other.myself)
			return false;
		return true;
	}
	
	

}
