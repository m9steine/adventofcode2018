package dec7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import utils.InputUtils;

public class Dec7x2 {

	private static final String DAY7_INPUT = "day7/input.txt";
	private static final String DAY7_TEST_INPUT = "day7/test.txt";

	private static List<Node> visitedNodes = new ArrayList<>();
	private static List<Node> evidence = new ArrayList<>();
	private static List<Node> availableNodes = new ArrayList<>();

	private static InputUtils helper = new InputUtils();

	public static void main(String[] args) {
		List<String> testInput = helper.getInput(DAY7_TEST_INPUT);
		List<String> input = helper.getInput(DAY7_INPUT);
		List<Worker> workers = new ArrayList<>();
		String result = "";
		int numberOfWorkers = 5;
		for (int index = 0; index < numberOfWorkers; index++) {
			workers.add(new Worker(index, 0));
		}

		List<Node> nodes = fillNodes(input);
		// List<Node> availableNodes = new ArrayList<>();
		// while (result.length() <= 5) {
		for (int second = 0; result.length() < 26; second++) {
			System.out.print("Sec: " + second + " ");

			for (Worker worker : workers) {
				if (second > worker.durationToCompleteTask) {
					if (worker.workingOn != 0) {
						evidence.add(new Node(worker.workingOn));
						result += worker.workingOn;
						worker.workingOn = 0;
					}

					worker.free = true;

					// worker.timeSpend = second;
					System.out.println("workerID:" + worker.id + " is free again");

					// break;
				}
				if (worker.free) {
					getAvailaibleNodes(nodes);
					// availableNodes.removeAll(visitedNodes);
					Collections.sort(availableNodes, new LexicographicComparator());
					int size = availableNodes.size();

					// TestOutput
					if(size > 0) {
						System.out.print("#availableNodes:"+availableNodes.size() + "--> ");
						for (Node node : availableNodes) {
							System.out.print(node.myself + ", ");
						}
						System.out.println();	
					}
					
					Node node = null;

					if (availableNodes.size() > 0) {
						node = availableNodes.get(0);
						worker.workingOn = node.myself;
						visitedNodes.add(node);
						availableNodes.remove(node);
						int workTill = (60 + (node.myself - 64) - 1) + second;
						worker.durationToCompleteTask = workTill;
						System.out.println("workerID:" + worker.id + " works on Node: " + node.myself + " till second: "
								+ workTill);
						worker.free = false;

					}
				} else {

				}

			}

		}
		/*
		 * getAvailaibleNodes(nodes); Collections.sort(availableNodes, new
		 * LexicographicComparator()); System.out.print(availableNodes.size()+"--> ");
		 * for (Node node : availableNodes) { System.out.print(node.myself+", "); }
		 * System.out.println();
		 */

		// }

		// }

		for (Worker worker : workers) {
			System.err.println("WorkerID:" + (worker.id + 1) + " worked" + (worker.durationToCompleteTask ));
		}

		System.err.println("Result Part 1: " + result);

	}

	private static void getAvailaibleNodes(List<Node> nodes) {
		nodes.removeAll(visitedNodes);
		boolean containsAllParents;
		if (evidence.isEmpty()) {
			for (Node node : nodes) {
				if (node.parent.isEmpty()) {
					// evidence.add(node);
					if(!availableNodes.contains(node)) {
						availableNodes.add(node);
					}
					
				}
			}
		} else {
			for (Node current : nodes) {
				containsAllParents = true;
				if (current.parent.isEmpty()) {
					containsAllParents = false;
				}

				for (Character parent : current.parent) {
					if (!(evidence.contains(new Node(parent)))) {
						containsAllParents = false;
					}

				}
				if (containsAllParents) {
					if (!availableNodes.contains(current)) {
						availableNodes.add(current);
					}

				}
			}
		}
	}

	private static List<Node> fillNodes(List<String> input) {
		List<Node> nodes = new ArrayList<>();
		String alphabetTest = "ABCDEF";
		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		for (int index = 0; index < alphabet.length(); index++) {
			nodes.add(new Node(alphabet.charAt(index)));
		}

		for (String line : input) {
			char myself = line.charAt(36);
			char parent = line.charAt(5);
			for (Node node : nodes) {
				if (node.myself == myself) {
					node.parent.add(parent);
				}
			}
		}
		for (Node node : nodes) {
			System.out.print(node.myself + "-->" + "Parents:");
			for (Character parent : node.parent) {
				System.out.print(parent + ",");
			}
			System.out.println();
		}

		return nodes;
	}

}

class LexicographicComparator implements Comparator<Node> {
	@Override
	public int compare(Node a, Node b) {
		if (a.myself > b.myself)
			return 1;
		else if (a.myself < b.myself)
			return -1;
		else
			return 0;
	}
}
