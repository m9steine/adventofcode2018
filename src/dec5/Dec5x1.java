package dec5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Dec5x1 {
	public static Set<String> reactedTo = new HashSet<>();
	public static ArrayList<Integer> count = new ArrayList<>();
	/*
	 * public ArrayList<Character> getReactedTo() { return reactedTo; }
	 * 
	 * public void setReactedTo(ArrayList<Character> reactedTo) { this.reactedTo =
	 * reactedTo; }
	 */

	public static ArrayList<String> getInput() throws IOException {
		FileReader fr = new FileReader("e:\\Input.txt");
		BufferedReader br = new BufferedReader(fr);
		String line = "";
		ArrayList<String> log = new ArrayList<String>();
		while ((line = br.readLine()) != null) {
			log.add(line);
		}
		br.close();
		return log;
	}

	public static String react(String polymer) {
		int check = 0;
		if (polymer.length() == 0) {
			return "";
		}
		char[] buf = polymer.toCharArray();
		for (int i = 1; i < buf.length; i++) {
			String test1 = Character.toString(buf[i]).toLowerCase();
			check = (buf[i] - buf[i - 1]);
			if (check == 32 || check == -32) {
				reactedTo.add(test1);
				buf[i] = 48;
				buf[i - 1] = 48;
				i++;

			}
		}
		String reactiteration = new String(buf);
		if (reactiteration.contains("0")) {
			reactiteration = reactiteration.replaceAll("0", "");
		} else {
			return reactiteration.substring(1, reactiteration.length() - 1);
		}

		return react(reactiteration);
	}

	public static String improvedPolymer(String polymer) {
		
		ArrayList<String> checkPruned = new ArrayList<>();
		String pruned = "";
		for (String letter : reactedTo) {
			pruned = polymer.replaceAll(letter.toString(), "");
			pruned = pruned.replaceAll(letter.toString().toUpperCase(), "");
			checkPruned.add(pruned);
		}
		
		for (String improved : checkPruned) {
			count.add(count(react(improved)));
		}
		
		return pruned;
	}

	public static int count(String reactedPolymer) {
		return reactedPolymer.length();
	}

	public static void main(String[] args) throws IOException {
		// ArrayList<String> input = getInput();
		System.out.println(count(react(getInput().toString())));
		react(improvedPolymer(getInput().toString()));
		System.out.println(Collections.min(count));
	}

}
