package dec2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Dec2x1 {

	public static ArrayList<String> getInput() throws IOException {
		FileReader fr = new FileReader("e:\\Input.csv");
		BufferedReader br = new BufferedReader(fr);
		String line = "";
		ArrayList<String> interference = new ArrayList<String>();
		while ((line = br.readLine()) != null) {
			interference.add(line);
		}
		br.close();
		return interference;
	}

	public static int countTwoTimes(ArrayList<String> input) {
		int sum = 0, cnt;
		for (String id : input) {
			cnt = 0;
			char[] text = id.toLowerCase().toCharArray();
			for (int i = 0; i < text.length; i++) {
				if (countLetter(id, text[i]) == 2) {
					cnt++;
				}
			}
			if (cnt > 1) {
				sum += cnt / cnt;
			} else {
				sum += cnt;
			}
		}
		return sum;
	}

	public static int countThreeTimes(ArrayList<String> input) {
		int sum = 0, cnt;
		for (String id : input) {
			cnt = 0;
			char[] text = id.toLowerCase().toCharArray();
			for (int i = 0; i < text.length; i++) {
				if (countLetter(id, text[i]) == 3) {
					cnt++;
				}
			}
			if (cnt > 1) {
				sum += cnt / cnt;
			} else {
				sum += cnt;
			}
		}
		return sum;
	}

	private static int countLetter(String str, char letter) {
		str = str.toLowerCase();
		letter = Character.toLowerCase(letter);
		int count = 0;

		for (int i = 0; i < str.length(); i++) {
			char currentLetter = str.charAt(i);
			if (currentLetter == letter)
				count++;
		}
		return count;
	}

	public static void main(String[] args) throws IOException {
		ArrayList<String> input = getInput();
		int containingTwo = countTwoTimes(input);
		int containingThree = countThreeTimes(input);

		System.out.println(containingTwo + "---" + containingThree);
		System.out.println(containingTwo * containingThree);
		
		for (String x : input) {
			compare(x,input);
		}

	}

	private static int compare(String x, ArrayList<String> input) {
		int result = 0;
		char[] xchar = x.toCharArray();
		for (String text : input) {
			result = 0;
			char[] textchar = text.toCharArray();
			for (int i = 0; i < xchar.length; i++) {
				if (xchar[i] != textchar[i]) {
					result += 1;
				}
			}
			if (result == 1) {
				System.out.println(x+"--"+text+":="+result);
			}
			
		}
		return result;
	}

}
