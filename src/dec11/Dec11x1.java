package dec11;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import utils.InputUtils;

public class Dec11x1 {

	private static final String DAY11_INPUT = "day11/input.txt";
	private static final String DAY11_TEST_INPUT = "day11/test.txt";

	private static InputUtils helper = new InputUtils();
	private static List<Integer> inputInt = new ArrayList<>();
	private static int xCoordinate = 0;
	private static int yCoordinate = 0;
	private static int maxPowerLevel = 0;
	private static int maxSize = 0;

	public static void main(String[] args) throws IOException {
		List<String> testInput = helper.getInput(DAY11_TEST_INPUT);
		List<String> input = helper.getInput(DAY11_INPUT);
		int[][] fuelGrid = new int[301][301];
		int serialNumber = Integer.parseInt(input.get(0));

		fuelGrid = fillFuelGrid(fuelGrid, serialNumber);
		findLargestPowerlevel(fuelGrid, 1);
		System.out.println(xCoordinate + "," + yCoordinate + "," + maxSize + ":=" + maxPowerLevel);

		xCoordinate = 0;
		yCoordinate = 0;
		maxPowerLevel = 0;

		findLargestPowerlevel(fuelGrid, 2);
		System.out.println(xCoordinate + "," + yCoordinate + "," + maxSize + ":=" + maxPowerLevel);

	}

	private static void findLargestPowerlevel(int[][] fuelGrid, int part) {
		switch (part) {
		case 1: {
			for (int index = 1; index < fuelGrid.length - 3; index++) {
				for (int jndex = 1; jndex < fuelGrid[index].length - 3; jndex++) {
					getPowerLevel(fuelGrid, index, jndex, 3);
				}
			}
			break;
		}
		case 2: {
			for (int index = 1; index < fuelGrid.length; index++) {
				for (int jndex = 1; jndex < fuelGrid[index].length; jndex++) {
					for (int size = 1; size < 300 - Math.max(index, jndex); size++) {
						getPowerLevel(fuelGrid, index, jndex, size);
					}
				}
			}
			break;

		}

		}

	}

	private static void getPowerLevel(int[][] fuelGrid, int i, int j, int size) {
		int max = 0;
		for (int index = i; index < size + i; index++) {
			for (int jndex = j; jndex < size + j; jndex++) {
				max += fuelGrid[index][jndex];
			}
		}
		if (max > maxPowerLevel) {
			maxPowerLevel = max;
			xCoordinate = i;
			yCoordinate = j;
			maxSize = size;

		}
	}

	private static int[][] fillFuelGrid(int[][] fuelGrid, int serialNumber) {
		for (int index = 1; index < fuelGrid.length; index++) {
			for (int jndex = 1; jndex < fuelGrid[index].length; jndex++) {
				int rackID = index + 10;
				int powerLevel = ((rackID * jndex) + serialNumber) * rackID;
				int hundredsDigit = 0;
				if (powerLevel >= 100) {
					hundredsDigit = (powerLevel / 100) % 10;
				}
				fuelGrid[index][jndex] = hundredsDigit - 5;
			}
		}
		return fuelGrid;
	}

}
