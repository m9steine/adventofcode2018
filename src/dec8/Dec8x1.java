package dec8;

import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

import utils.InputUtils;

public class Dec8x1 {

	private static final String DAY8_INPUT = "day8/input.txt";
	private static final String DAY8_TEST_INPUT = "day8/test.txt";

	private static Set<Character> visitedNodes = new HashSet<>();
	private static InputUtils helper = new InputUtils();

	private static List<Node> nodes = new ArrayList<>();
	private static List<Integer> inputInt = new ArrayList<>();
	private static int index = 0;

	public static void main(String[] args) {
		List<String> testInput = helper.getInput(DAY8_TEST_INPUT);
		List<String> input = helper.getInput(DAY8_INPUT);
		String inputLine = input.toString().substring(1, input.toString().length() - 1);
		int sumMetaData = 0;
		String[] inputArr = inputLine.split(" ");
		int[] intArr = new int[inputArr.length];
		for (int index = 0; index < inputArr.length; index++) {
			intArr[index] = Integer.parseInt(inputArr[index]);
			inputInt.add(Integer.parseInt(inputArr[index]));
		}

		sumMetaData = getMetaData(intArr, 0);
		System.err.println("Result part 1: " + sumMetaData);

		getChildren(null);

		sumMetaData = 0;
		Node root = nodes.get(0);
		
		sumMetaData = getValueofMetadata(root, 0);
		
		

		System.err.println("Result part 2: " + sumMetaData);

	}

	private static int getValueofMetadata(Node current, int sum) {
		if(current.getNumChildren() == 0) {
			sum += current.getSumOfMetadata();
		}else {
			for (int meta : current.getMetaData()) {
				if ((meta - 1) < current.getNumChildren()) {
					sum = getValueofMetadata(nodes.get((current.getChildren().get(meta-1).id)-1), sum);
				} else {
					sum += 0;
				}
			}
		}
		
		return sum;
	}

	private static void getChildren(Node node) {
		int children = 0, metaData = 0;
		Node current = new Node();
		nodes.add(current);
		children = inputInt.get(index);
		metaData = inputInt.get(index + 1);
		current.setNumMetadata(metaData);
		if (children > 0) {
			current.setNumChildren(children);
			index++;
			index++;
			for (int jndex = 0; jndex < children; jndex++) {
				getChildren(current);
			}
			for (int jndex = 0; jndex < metaData; jndex++) {
				current.getMetaData().add(inputInt.get(index + jndex));
			}
			if (node != null) {
				node.getChildren().add(current);
			}

			index += metaData;

		} else {
			index++;
			index++;
			for (int jndex = 0; jndex < metaData; jndex++) {
				current.setSumOfMetadata(current.getSumOfMetadata() + inputInt.get(index + jndex));
				current.getMetaData().add(inputInt.get(index + jndex));

			}
			node.getChildren().add(current);
			index += metaData;
			return;
		}

	}

	public static int getMetaData(int[] line, int sumMetaData) {
		if (line.length == 0) {
			return sumMetaData;
		}

		for (int index = 0; index < line.length; index++) {
			int childs = 0, metaData = 0;
			childs = line[index];
			if (childs == 0) {
				line = removeTheElement(line, index);
				metaData = line[index];
				line = removeTheElement(line, index);

				for (int jndex = metaData; jndex > 0; jndex--) {
					sumMetaData += line[index];
					line = removeTheElement(line, index);

				}
				if (index != 0) {
					line[index - 2]--;
				}

				index--;

			}
		}
		return getMetaData(line, sumMetaData);
	}

	public static int[] removeTheElement(int[] arr, int index) {

		// If the array is empty
		// or the index is not in array range
		// return the original array
		if (arr == null || index < 0 || index >= arr.length) {

			return arr;
		}

		// return the resultant array
		return IntStream.range(0, arr.length).filter(i -> i != index).map(i -> arr[i]).toArray();
	}

}
