package dec8;

import java.util.ArrayList;
import java.util.List;

public class Node {
	
	static int number = 0;
	int id;
	private int numChildren;
	private int numMetadata;
	private int sumOfMetadata;
	private List<Node> children = new ArrayList<>();
	private List<Integer> metaData = new ArrayList<>();
	
	Node(){
		number++;
		this.id = number;
	}
	
	
	public int getNumChildren() {
		return numChildren;
	}
	
	public void setNumChildren(int numChildren) {
		this.numChildren = numChildren;
	}
	public int getNumMetadata() {
		return numMetadata;
	}
	public void setNumMetadata(int numMetadata) {
		this.numMetadata = numMetadata;
	}
	public List<Node> getChildren() {
		return children;
	}
	public void setChildren(List<Node> children) {
		this.children = children;
	}
	public List<Integer> getMetaData() {
		return metaData;
	}
	public void setMetaData(List<Integer> metaData) {
		this.metaData = metaData;
	}


	public int getSumOfMetadata() {
		return sumOfMetadata;
	}


	public void setSumOfMetadata(int sumOfMetadata) {
		this.sumOfMetadata = sumOfMetadata;
	}
	


}
