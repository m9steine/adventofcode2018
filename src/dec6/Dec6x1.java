package dec6;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Dec6x1 {

	public static ArrayList<String> getInput() throws IOException {
		FileReader fr = new FileReader("e:\\Input.txt");
		BufferedReader br = new BufferedReader(fr);
		String line = "";
		ArrayList<String> log = new ArrayList<String>();
		while ((line = br.readLine()) != null) {
			log.add(line);
		}
		br.close();
		return log;
	}

	public static void printGrid(int[][] coordinateSystem) {
		for (int i = 0; i < coordinateSystem.length; i++) {
			for (int j = 0; j < coordinateSystem[i].length; j++) {
				if (coordinateSystem[i][j] < 10000) {
					System.out.print("  " + coordinateSystem[i][j]);
				} else {
					System.out.print(" " + coordinateSystem[i][j]);
				}

			}
			System.out.println();
		}
	}

	public static void fillGrid(int[][] coordinateSystem) {
		for (int i = 0; i < coordinateSystem.length; i++) {
			for (int j = 0; j < coordinateSystem[i].length; j++) {
				coordinateSystem[i][j] = 0;
			}
		}
	}

	private static int getMinManhattanDistance(ArrayList<String> coordinates, int xstart, int ystart) {
		int xgoal = 0, ygoal = 0;
		int minDistance = 800;
		int indexOfNearestCoordinate = 1, index = 1;
		for (String coordinate : coordinates) {

			xgoal = Integer.parseInt(coordinate.substring(0, coordinate.indexOf(",")));
			ygoal = Integer.parseInt(coordinate.substring(coordinate.indexOf(",") + 2, coordinate.length()));
			int actualDistance = Math.abs(xgoal - xstart) + Math.abs(ygoal - ystart);
			if (actualDistance < minDistance) {
				minDistance = actualDistance;
				indexOfNearestCoordinate = index;
			} else if (actualDistance == minDistance) {
				indexOfNearestCoordinate = 0;
			}
			index++;
		}

		return indexOfNearestCoordinate;
	}

	private static int getSumManhattanDistance(ArrayList<String> coordinates, int xstart, int ystart) {
		int xgoal = 0, ygoal = 0;
		int sumDistance = 0;
		for (String coordinate : coordinates) {

			xgoal = Integer.parseInt(coordinate.substring(0, coordinate.indexOf(",")));
			ygoal = Integer.parseInt(coordinate.substring(coordinate.indexOf(",") + 2, coordinate.length()));
			int actualDistance = Math.abs(xgoal - xstart) + Math.abs(ygoal - ystart);
			sumDistance += actualDistance;
		}

		return sumDistance;
	}

	private static HashMap<Integer, Integer> deleteInfinityAreas(HashMap<Integer, Integer> area,
			int[][] coordinateSystem) {
		for (int i = 0; i < coordinateSystem.length; i++) {

			/*
			 * for Testing, because 8 is removed, but no infinit area. int w =
			 * coordinateSystem[0][i]; int x = coordinateSystem[i][0]; int y =
			 * coordinateSystem[coordinateSystem.length-1][i]; int z =
			 * coordinateSystem[i][coordinateSystem.length-1];
			 * 
			 * if( z == 8 ) { System.out.print(i+"-");
			 * System.out.println(coordinateSystem.length-1); }
			 * 
			 */

			area.remove(coordinateSystem[0][i]);
			area.remove(coordinateSystem[i][0]);
			area.remove(coordinateSystem[coordinateSystem.length - 1][i]);
			area.remove(coordinateSystem[i][coordinateSystem.length - 1]);

		}

		return area;
	}

	public static void main(String[] args) throws IOException {

		ArrayList<String> coordinates = getInput();
		HashMap<Integer, Integer> area = new HashMap<>();

		int[][] coordinateSystem = new int[313][313];

		for (int i = 0; i < coordinateSystem.length; i++) {
			for (int j = 0; j < coordinateSystem[i].length; j++) {
				coordinateSystem[j][i] = getMinManhattanDistance(coordinates, i, j);
				if (area.containsKey(coordinateSystem[j][i])) {
					area.put(coordinateSystem[j][i], area.get(coordinateSystem[j][i]).intValue() + 1);
				} else {
					area.put(coordinateSystem[j][i], 1);
				}
			}
		}

		area = deleteInfinityAreas(area, coordinateSystem);
		int sizeOfLargestArea = 0;
		for (Map.Entry entry : area.entrySet()) {
			int size = (int) entry.getValue();
			if (size > sizeOfLargestArea) {
				sizeOfLargestArea = size;
			}
		}

		System.out.println("Result part one: " + sizeOfLargestArea);

		int[][] coordinateSystem2 = new int[313][313];
		int sizeOfRegion = 0;
		for (int i = 0; i < coordinateSystem2.length; i++) {
			for (int j = 0; j < coordinateSystem2[i].length; j++) {
				coordinateSystem2[j][i] = getSumManhattanDistance(coordinates, i, j);
				if (coordinateSystem2[j][i] < 10000) {
					sizeOfRegion++;
				}

			}
		}

		System.out.println("Result part two: " + sizeOfRegion);

		/*
		 * Output for testing. System.setOut(new PrintStream(new
		 * File("E:/output.txt"))); printGrid(coordinateSystem2);
		 */
	}

}
