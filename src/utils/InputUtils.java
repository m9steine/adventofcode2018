package utils;

import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class InputUtils {
	
	public List<String> getInput(String fileName)  {
		
		URL resource = ClassLoader.getSystemResource(fileName);
		try {
			URI uri =resource.toURI();
			Path path = Paths.get(uri);
			return Files.lines(path).collect(Collectors.toList());
		} catch (Exception e) {
			System.exit(1);
		}
		
		return null;
	}

}
