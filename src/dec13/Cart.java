package dec13;

public class Cart {

	static int number = 0;

	enum Direction {
		left, right, down, up
	}

	int id;
	int xPos = 0;
	int yPos = 0;
	int intersection = 0;
	Direction direction;

	public Cart(int index, int jndex, char direction2) {
		number++;
		this.id = number;
		this.xPos = index;
		this.yPos = jndex;
		switch (direction2) {
		case '<':
			this.direction = Direction.left;
			break;
		case '>':
			this.direction = Direction.right;
			break;
		case 'v':
			this.direction = Direction.down;
			break;
		case '^':
			this.direction = Direction.up;
			break;
		}
	}

	public void setNextCoordinates(int[] newPosition) {
		this.xPos = newPosition[0];
		this.yPos = newPosition[1];
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public void passedIntersection() {
		this.intersection += 1;
	}

}
