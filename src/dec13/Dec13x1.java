package dec13;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import dec13.Cart.Direction;
import utils.InputUtils;

public class Dec13x1 {

	private static final String DAY13_INPUT = "day13/input.txt";
	private static final String DAY13_TEST_INPUT = "day13/test.txt";
	private static InputUtils helper = new InputUtils();

	private static char[][] playfield = new char[150][150];
	private static List<Cart> carts = new ArrayList<Cart>();
	private static boolean collision = false;
	private static List<Integer> cartsToDelete = new ArrayList<>();
	private static boolean allowedToDelete = false;

	public static void main(String[] args) {
		List<String> testInput = helper.getInput(DAY13_TEST_INPUT);
		List<String> input = helper.getInput(DAY13_INPUT);
		initPlayfield(input);
		printPlayfield();

		while (!collision) {
			makeMove();

			if (allowedToDelete) {
				allowedToDelete = false;
				deleteFromCarts(cartsToDelete);
				if (carts.size() == 1) {
					collision = true;
					System.out.println(carts.get(0).yPos + "," + carts.get(0).xPos);
				}
			}

			carts.sort(new Comparator<Cart>() {

				@Override
				public int compare(Cart first, Cart second) {
					if (first.xPos < second.xPos) {
						return -1;
					}
					if (first.xPos > second.xPos) {
						return 1;
					}
					if (first.yPos < second.yPos) {
						return -1;
					}
					if (first.yPos > second.yPos) {
						return 1;
					}
					return 0;
				}

			});

		}

	}

	private static void deleteFromCarts(List<Integer> cartsToDelete2) {
		Collections.sort(cartsToDelete2, Collections.reverseOrder());

		for (Integer index : cartsToDelete2) {
			carts.remove((int) index);
		}
		cartsToDelete = new ArrayList<>();
	}

	private static void checkForCollision() {
		for (Cart current : carts) {
			for (Cart next : carts) {
				if (!current.equals(next) && current.xPos == next.xPos && current.yPos == next.yPos) {
					collision = true;
				}
			}
		}
	}

	private static void deleteCollidingCarts() {

		for (Cart current : carts) {
			for (Cart next : carts) {
				if (!current.equals(next) && current.xPos == next.xPos && current.yPos == next.yPos) {
					if (!cartsToDelete.contains(carts.indexOf(current))) {
						cartsToDelete.add(carts.indexOf(current));
					}
					allowedToDelete = true;
				}
			}
		}
	}

	public static void initPlayfield(List<String> input) {
		int index = 0;
		for (String line : input) {
			char[] row = line.toCharArray();
			for (int jndex = 0; jndex < row.length; jndex++) {
				char cell = row[jndex];
				if (cell == '<' || cell == '>') {
					Cart cart = new Cart(index, jndex, cell);
					carts.add(cart);
					playfield[index][jndex] = '-';
				} else if (cell == 'v' || cell == '^') {
					Cart cart = new Cart(index, jndex, cell);
					carts.add(cart);
					playfield[index][jndex] = '|';
				} else {
					playfield[index][jndex] = cell;
				}
			}

			index++;
		}
	}

	private static void makeMove() {

		for (Cart current : carts) {
			checkCell(current, getNextCoordinates(current));
		}

	}

	private static void checkCell(Cart current, int[] nextCoordinates) {
		/**
		 * checkForCollision() is for the output of part1
		 */
		// checkForCollision();

		deleteCollidingCarts();
		char cell = playfield[current.xPos][current.yPos];
		switch (current.direction) {
		case left:
			if (cell == '-') {
			} else if (cell == '\\') {
				current.setDirection(Direction.up);
			} else if (cell == '/') {
				current.setDirection(Direction.down);
			} else if (cell == '+') {
				switch (current.intersection % 3) {
				case 0:
					current.setDirection(Direction.down);
					current.passedIntersection();
					break;
				case 1:
					current.passedIntersection();
					break;
				case 2:
					current.passedIntersection();
					current.setDirection(Direction.up);
					break;
				}
			}
			break;
		case right:
			if (cell == '-') {
			} else if (cell == '\\') {
				current.setDirection(Direction.down);
			} else if (cell == '/') {
				current.setDirection(Direction.up);
			} else if (cell == '+') {
				switch (current.intersection % 3) {
				case 0:
					current.setDirection(Direction.up);
					current.passedIntersection();
					break;
				case 1:
					current.passedIntersection();
					break;
				case 2:
					current.passedIntersection();
					current.setDirection(Direction.down);
					break;
				}
			}
			break;
		case up:
			if (cell == '|') {
			} else if (cell == '\\') {
				current.setDirection(Direction.left);
			} else if (cell == '/') {
				current.setDirection(Direction.right);
			} else if (cell == '+') {
				switch (current.intersection % 3) {
				case 0:
					current.setDirection(Direction.left);
					current.passedIntersection();
					break;
				case 1:
					current.passedIntersection();
					break;
				case 2:
					current.passedIntersection();
					current.setDirection(Direction.right);
					break;
				}
			}
			break;
		case down:
			if (cell == '|') {
			} else if (cell == '\\') {
				current.setDirection(Direction.right);
			} else if (cell == '/') {
				current.setDirection(Direction.left);
			} else if (cell == '+') {
				switch (current.intersection % 3) {
				case 0:
					current.setDirection(Direction.right);
					current.passedIntersection();
					break;
				case 1:
					current.passedIntersection();
					break;
				case 2:
					current.passedIntersection();
					current.setDirection(Direction.left);
					break;
				}
			}

			break;
		}
	}

	private static int[] getNextCoordinates(Cart current) {
		int[] move = new int[2];
		switch (current.direction) {
		case left:
			move[0] = 0;
			move[1] = -1;
			break;
		case right:
			move[0] = 0;
			move[1] = 1;
			break;
		case up:
			move[0] = -1;
			move[1] = 0;
			break;
		case down:
			move[0] = 1;
			move[1] = 0;
			break;
		}
		current.xPos += move[0];
		current.yPos += move[1];

		return move;
	}

	private static void printPlayfield() {
		for (int index = 0; index < playfield.length; index++) {
			for (int jndex = 0; jndex < playfield[index].length; jndex++) {
				System.out.print(playfield[index][jndex]);
			}
			System.out.println();
		}
	}

}
